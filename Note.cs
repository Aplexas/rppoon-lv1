using System;
using System.Collections.Generic;
using System.Text;




namespace LV_1
{
    class Note
    {
        private String noteText;
        private int levelOfImportance;
        private String authorName;

        public Note()
        {
            this.noteText = "None";
            this.authorName = "None";
            this.levelOfImportance = 0;
        }
        
        public Note(int levelOfImportance)
        {
            this.levelOfImportance = levelOfImportance;
            this.noteText = "None";
            this.authorName = "None";
        }

        public Note(String noteText, String authorName,int levelOfImportance)
        {
            this.noteText = noteText;
            this.levelOfImportance = levelOfImportance;
            this.authorName = authorName;
        }

        public String getNoteText()
        {
            return this.noteText;
        }

        public String getAuthorName()
        {
            return this.authorName;
        }
        
        public int getLevelOfImportance()
        {
            return this.levelOfImportance;
        }

        public void setNoteText(String noteText)
        {
            this.noteText = noteText;
        }

        public void setAuthorName(String authorName)
        {
            this.authorName = authorName;
        }

        public void setLevelOfImportance(int levelOfImportance)
        {
            this.levelOfImportance= levelOfImportance;
        }

        public String NoteText {
            get { return this.noteText; }
            set { this.noteText = value;  }
        }

        public String AuthorName
        {
            get { return this.authorName; }
            set { this.authorName = value; }
        }

        public int LevelOfImportance
        {
            get { return this.levelOfImportance; }
            set { this.levelOfImportance = value; }
        }

        public override string ToString()
        {
            return this.AuthorName+" , razina važnosti: "+ this.LevelOfImportance;
        }
    }
}
